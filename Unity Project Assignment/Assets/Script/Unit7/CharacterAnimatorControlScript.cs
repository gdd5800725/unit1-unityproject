﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Script.Unit7
{
    public class CharacterAnimatorControlScript : MonoBehaviour
    {
        [SerializeField]private Slider _slider_forword;
        [SerializeField]private Slider _slider_turn;
        [SerializeField]private TMP_Dropdown _tmpDropdown;
        [SerializeField] private Toggle _toggle;
        [SerializeField] private Toggle use_ui_for_Contorll;
        
        protected Animator m_Animator;
        private Rigidbody _rigidbody;
        private float speed = 0.5f;
        private bool is_move_forward = true;
        
        private static readonly int Punch = Animator.StringToHash("Fire");
        private static readonly int Dancing = Animator.StringToHash("Dancing");
        private static readonly int State = Animator.StringToHash("State");
        private static readonly int Turn = Animator.StringToHash("Turn");

        private void Start()
        {
            m_Animator = GetComponent<Animator>();
            _rigidbody = GetComponent<Rigidbody>();
        }

        void Update()
        {
            if (_slider_forword != null)
            {
                print("Select anim : " + _tmpDropdown.value);
                if (Input.GetKeyDown(KeyCode.Space) || _tmpDropdown.value == 3)
                {
                    m_Animator.SetTrigger("Jump");
                }

                if (Input.GetKeyDown(KeyCode.Z) || _tmpDropdown.value == 2)
                {
                    m_Animator.SetTrigger(Punch);
                }

                if (Input.GetKeyDown(KeyCode.X) || _tmpDropdown.value == 1)
                {
                    m_Animator.SetTrigger(Dancing);
                }

                if (Input.GetKeyDown(KeyCode.C))
                {
                    m_Animator.SetInteger(State, 2);
                }

                if (Input.GetKeyDown(KeyCode.LeftShift) || (_toggle.isOn && use_ui_for_Contorll.isOn))
                {
                    if (is_move_forward)
                    {
                        m_Animator.SetFloat("Forward", speed * 2);
                    }
                    else
                    {
                        m_Animator.SetFloat("Forward", speed * -2);
                    }
                }

                if ((Input.GetKeyUp(KeyCode.LeftShift) && Input.GetKeyDown(KeyCode.W)) ||
                    (!_toggle.isOn && _slider_forword.value <= 0.5f && _slider_forword.value > 0))
                {
                    m_Animator.SetFloat("Forward", speed);
                }
                else if (Input.GetKeyUp(KeyCode.LeftShift) || !_toggle.isOn && use_ui_for_Contorll.isOn)
                {
                    m_Animator.SetFloat("Forward", 0);
                }

                if (Input.GetKeyUp(KeyCode.LeftShift) && Input.GetKeyDown(KeyCode.S) ||
                    (!_toggle.isOn && _slider_forword.value >= -0.5f && _slider_forword.value < 0))
                {
                    m_Animator.SetFloat("Forward", -speed);
                }
                else if (Input.GetKeyUp(KeyCode.LeftShift) || !_toggle.isOn && use_ui_for_Contorll.isOn)
                {
                    m_Animator.SetFloat("Forward", 0);
                }

                if (use_ui_for_Contorll.isOn)
                {
                    if (!_toggle.isOn)
                        m_Animator.SetFloat("Forward", _slider_forword.value);

                    if (_slider_forword.value >= 0)
                    {
                        is_move_forward = true;
                    }
                    else
                    {
                        is_move_forward = false;
                    }

                    m_Animator.SetFloat(Turn, _slider_turn.value);
                }

                if (Input.GetKeyDown(KeyCode.W))
                {
                    m_Animator.SetFloat("Forward", speed);
                    is_move_forward = true;
                }

                if (Input.GetKeyUp(KeyCode.W))
                {
                    m_Animator.SetFloat("Forward", 0);
                }

                if (Input.GetKeyDown(KeyCode.S))
                {
                    m_Animator.SetFloat("Forward", -speed);
                    is_move_forward = false;
                }

                if (Input.GetKeyUp(KeyCode.S))
                {
                    m_Animator.SetFloat("Forward", 0);
                }

                if (Input.GetKeyDown(KeyCode.D))
                {
                    m_Animator.SetFloat(Turn, 0.5f);
                }

                if (Input.GetKeyUp(KeyCode.D))
                {
                    m_Animator.SetFloat(Turn, 0);
                }

                if (Input.GetKeyDown(KeyCode.A))
                {
                    m_Animator.SetFloat(Turn, -0.5f);
                }

                if (Input.GetKeyUp(KeyCode.A))
                {
                    m_Animator.SetFloat(Turn, 0);
                }
            }
        }
    }
}