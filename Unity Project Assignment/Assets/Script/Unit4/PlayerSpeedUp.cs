using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class PlayerSpeedUp : MonoBehaviour
{
    private CapsulePlayerController _playerController;

    private string s = "";

    private bool isPowerUp = false;
    // Start is called before the first frame update
    void Start()
    {
        _playerController = FindObjectOfType<CapsulePlayerController>();
    }

    void OnGUI()
    {
        if (isPowerUp)
        {
            GUI.color = Color.red;
        }
        else
        {
            GUI.color = Color.yellow;
        }
        
        GUI.Label(new Rect(720, 5, 150, 50), s);
    }
    
    public void SpeedUp(float power)
    {
        isPowerUp = true;
        _playerController._dDirectionalSpeed += power;
        _playerController._DirectionalSprintSpeed += power;
        s = "Power UP!!!!!!!";
    }
    
    public void SpeedDown(float power)
    {
        isPowerUp = false;
        _playerController._dDirectionalSpeed -= power;
        _playerController._DirectionalSprintSpeed -= power;
        s = "Power Down......";
    }
}
