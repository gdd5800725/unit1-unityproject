﻿using GDD;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Script.Unit5
{
    [RequireComponent(typeof(Rigidbody))]
    public class PushableObjectWithTimer : MonoBehaviour, IInteractable, IActorEnterExitHandler
    {
        [SerializeField] protected TextMeshProUGUI m_InteractionTxt;
        [SerializeField] protected float m_Power = 10;
        [SerializeField] protected float m_TimerDuration = 5;
        [SerializeField] protected Slider m_SliderTimer;
        private bool _IsTimerStart = false;
        private float _StartTimeStamp;
        private float _EndTimeStamp;
        private float _SliderValue;
        private Rigidbody m_rigidBody;
        private GameObject m_push_actor;
        private bool isPushActor;

        private void Start()
        {
            m_rigidBody = GetComponent<Rigidbody>();
        }

        public void Interact(GameObject actor)
        {
            OnPush(actor);
        }

        public void ActorEnter(GameObject actor)
        {
            m_InteractionTxt.gameObject.SetActive(true);
        }

        public void ActorExit(GameObject actor)
        {
            m_InteractionTxt.gameObject.SetActive(false);
        }
        
        private void StartTimer()
        {
            //Check if the timer is already running
            if (_IsTimerStart) return;
            _IsTimerStart = true;
            _StartTimeStamp = Time.time;
            _EndTimeStamp = Time.time + m_TimerDuration;
            _SliderValue = 0;
        }

        private void Update()
        {
            if (!_IsTimerStart) return;

            _SliderValue = ((Time.time - _StartTimeStamp) / m_TimerDuration) *
                           m_SliderTimer.maxValue;
            m_SliderTimer.value = _SliderValue;

            if (Time.time >= _EndTimeStamp && isPushActor)
            {
                m_rigidBody.AddForce(m_push_actor.transform.forward * m_Power, ForceMode.Impulse);
                isPushActor = false;
                _IsTimerStart = false;
            }
        }

        public void OnPush(GameObject actor)
        {
            //Start the timer
            StartTimer();
            isPushActor = true;
            m_push_actor = actor;
        }
    }
}
