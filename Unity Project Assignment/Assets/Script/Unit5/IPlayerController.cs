namespace GDD
{
    public interface IPlayerController
    {
        void MoveForward();
        void MoveForwardSprint();

        void MoveBackward();

        void TurnLeft();
        void TurnRight();
    }
}