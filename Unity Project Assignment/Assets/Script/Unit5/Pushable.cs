﻿using GDD;
using TMPro;
using UnityEngine;

namespace Script.Unit5
{
    [RequireComponent(typeof(Rigidbody))]
    public class Pushable : MonoBehaviour, IInteractable, IActorEnterExitHandler
    {
        [SerializeField] protected TextMeshProUGUI m_InteractionTxt;
        [SerializeField] protected float m_Power = 10;
        private Rigidbody m_rigidBody;

        private void Start()
        {
            m_rigidBody = GetComponent<Rigidbody>();
        }
        
        public void Interact(GameObject actor)
        {
            //OnPush(actor);
        }

        public void ActorEnter(GameObject actor)
        {
            m_InteractionTxt.gameObject.SetActive(true);
        }

        public void ActorExit(GameObject actor)
        {
            m_InteractionTxt.gameObject.SetActive(false);
        }

        public void OnPush(GameObject actor)
        {
            m_rigidBody.AddForce(actor.transform.forward * m_Power, ForceMode.Impulse);
        }
    }
}
