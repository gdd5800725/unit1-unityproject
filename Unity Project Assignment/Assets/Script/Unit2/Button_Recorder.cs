using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.Serialization;
using UnityEngine.UI;

public class Button_Recorder : MonoBehaviour
{
   [SerializeField]private PlayerInput _playerInput;
   [SerializeField] private Text Output_Text;
  
   private Queue<string> m_listkeyofpressed = new Queue<string>();
   private bool presskey = false;

   [SerializeField] private double timeOut = 10.0f;
   private double timestamp;
   private string outputtimeout = "5 Keys in 10 Sec ";
   private String allkey;
   
   private void Update()
   {
      if (timestamp <= timeOut && timestamp > 0.0f)
      {
         outputtimeout = "5 Keys in " + (int)timestamp + " Sec ";
         timestamp -= Time.deltaTime;
         OnRecord();
      }
      else if (timestamp <= 0.0f)
      {
         timestamp = timeOut;
         int index = 0;
         allkey = null;

         foreach (var key in m_listkeyofpressed)
         {
            index++;
            allkey += " : " + index + ". " + key;
         }

         Debug.Log(outputtimeout + allkey);
         m_listkeyofpressed = new Queue<string>();
      }
      
      Output_Text.text = outputtimeout + allkey;
   }

   private void OnRecord()
   {
      foreach (KeyCode keycode in Enum.GetValues(typeof(KeyCode)))
      {
         if (Input.GetKeyDown(keycode))
         {
            if (!presskey && m_listkeyofpressed.Count < 5)
            {
               presskey = true;
               m_listkeyofpressed.Enqueue(keycode.ToString());
            }
            
            //print(keycode.ToString() + " : " + m_listkeyofpressed[m_listkeyofpressed.Count - 1]);
         }
         else
         {
            presskey = false;
         } 
      }
   }
}
