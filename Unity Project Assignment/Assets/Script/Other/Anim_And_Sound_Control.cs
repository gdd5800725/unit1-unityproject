using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

namespace GDD
{
    public class Anim_And_Sound_Control : MonoBehaviour
    {
        [SerializeField] private Animator m_animator;
        [SerializeField] private AudioSource m_audioSource;
        [SerializeField] private List<AudioClip> m_audioClips;
        private int index = 0;

        private void PlayNextAnimation()
        {
            m_audioSource.Stop();
            if (m_audioClips[index] != null)
            {
                m_audioSource.clip = m_audioClips[index];
                m_audioSource.Play();
            }

            index++;

            if (index >= m_animator.GetBehaviours<Anim_SMB>().Length)
            {
                index = 0;
            }
            m_animator.SetInteger("AnimSelect", index);
        }

        private void Start()
        {
            print(m_animator.GetBehaviours<Anim_SMB>().Length);
            foreach (var animationClip in m_animator.GetBehaviours<Anim_SMB>())
            {
                animationClip.StateEnterAction += PlayNextAnimation;
                //print("GGG : " + animationClip.name + " : " + animationClip);
            }
        }
    }
}